//-------------------------- Express ------------------------//

const express			= require('express');
const bodyParser  = require('body-parser');
const db					= require('./mongodb.js');

const app 				= express();
//-----------------------------------------------------------


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//------------------

 // app.use(express.static(__dirname + '/public'));
//-----------------------------------------------------------




// app.get('/', function(req, res) {
//   res.render('index.ejs');
// });




/*Делаем ТОЧНЫЙ запрос..
* в поиске юзера прописываем в заголовке запроса
 surch: "данные", получаем массив с данными, если юзер не 
 один не найден, вернется пустой массив

 *с добавлении юзера, прописываем точно инфу в json, который
 потом отправляем, ответ будет либо ошибка, либо текст,
 вернет текст, либо ошибку

 *обновляю юзера, делем запрос.. с телом в котором содержатся
 новые параметры.. бд сама найдет юзера по id тела и обновит 
 параметры.., вернет текст, либо ошибку 

 *удаление происходит по id.. Запрос DELETE делаем с телом..
 {id: 'айдишник'}, остальные параметры игнорятся,
 вернет текст, либо ошибку
 ...

 */
 app.route('/user')
 .get(function (req, res) {
  db.surch_user(req.headers.surch).then(data => {
    console.log(data);
    res.send(data);
  });
})
 .post(function (req, res) {
   db.add_user(req.body).then(data => {
    console.log(data);
    res.send(data);
  });
 })
 .put(function(req,res) {
  db.update_user(req.body.id, req.body).then(data => {
    console.log(data);
    res.send(data);
  })
})
 .delete(function(req, res) {
    db.delete_user(req.body.id).then(data => {
    console.log(data);
    res.send(data);
    })
  })





//-----------------------------------------------------------


module.exports = app;