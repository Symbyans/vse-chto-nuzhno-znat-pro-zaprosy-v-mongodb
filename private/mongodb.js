

const MongoClient = require('mongodb').MongoClient;
const url 				= 'mongodb://localhost:27017/ex_db';
//-------------------------------------------------------
//-------------------------------------------------------
//-------------------------------------------------------



function add_user(data){
	return new Promise((res, rej) => {
		MongoClient.connect( url, function( err, db ) {
			if( err ) {
				rej( err );
			} else {
				let collection 	= db.collection('users');
				collection.insert(data, function( err, result ) {
					if( err ){
						rej( err );
					} else { 
						res( 'user is added' );
					}
				});
				db.close();
			}
		});
	});
};
//-------------------------------------------------------



 function surch_user(data) {
 	return new Promise((res, rej) => {
 		MongoClient.connect( url, function( err, db ) {
 			if( err ) {
 				rej(err);
 			} else {
 				let collection 	= db.collection('users');
 				collection.find( {
 					$or:[
 					{ "first_name" : {$regex: data} },
 					{ "last_name" : {$regex: data} },
 					{ "number_phone" : {$regex: data} }
 					]
 				} ).toArray(function( err, result ) {

 					if( err ) {
 						rej(err);
 					} else {
 						res(result);
 					};
 				});
 			// collection.drop();
 			db.close();
 		}
 	});
 	});
 };
//-------------------------------------------------------



function update_user(id, new_data) {
	return new Promise((res, rej) =>{
		MongoClient.connect( url, function( err, db ) {
			if( err ) {
				rej(err);
			} else {
				let collection 	= db.collection('users');
				collection.update({'id': id}, {$set: new_data} );
				res('user is updated');
				db.close();
			}
		});
	});
};
//-------------------------------------------------------



function delete_user(id) {
	return new Promise((res, rej) => {
		MongoClient.connect( url, function( err, db ) {
			if( err ) {
				rej(err);
			} else {
				let collection 	= db.collection('users');
				collection.remove({'id': id});
				res('user is deleted');
				db.close();
			}
		});
	});
};




//-------------------------------------------------------
//-------------------------------------------------------
//-------------------------------------------------------
module.exports = {
	surch_user,
	add_user,
	update_user,
	delete_user
};